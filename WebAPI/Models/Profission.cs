﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Profission
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }

        public Profission()
        {
        }

        public Profission(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
