﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class ProfileDevContext : DbContext
    {
        public ProfileDevContext(DbContextOptions<ProfileDevContext> options) : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Profission> Profission { get; set; }
    }
}
