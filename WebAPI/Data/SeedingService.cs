﻿using System.Linq;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class SeedingService
    {
        private ProfileDevContext _context;

        public SeedingService(ProfileDevContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if(_context.User.Any() || _context.Profission.Any())
            {
                return;
            }

            Profission p1 = new Profission(1, "Back-End");
            Profission p2 = new Profission(2, "Font-End");
            Profission p3 = new Profission(3, "FullStack");
            Profission p4 = new Profission(4, "Test");

            User u1 = new User(1, "Paulo Modesto", "paulo@gmail.com", "123456", p3);
            User u2 = new User(2, "Joao MArcelo", "joao@gmail.com", "123456", p2);
            User u3 = new User(3, "Carlos Eduardo", "carlos@gmail.com", "123456", p1);
            User u4 = new User(4, "Lucas Lima", "lucas@gmail.com", "123456", p1);
            User u5 = new User(5, "Alex Grey", "alex@gmail.com", "123456", p4);

            _context.Profission.AddRange(p1, p2, p3, p4);
            _context.User.AddRange(u1, u2, u3, u4, u5);

            _context.SaveChanges();
        }
    }
}
