﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class UserService
    {
        private readonly ProfileDevContext _context;

        public UserService(ProfileDevContext context)
        {
            _context = context;
        }

        public List<User> FindAllUser()
        {
            var list = _context.User.ToList<User>();
            return list;
        }

        public User FindByUser(int id)
        {
            var user = _context.Find<User>(id);
            return user;
        }
    }
}
