﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public List<User> FindAll()
        {
            var list = _userService.FindAllUser();
            return list;
        }

        [HttpGet("{id}")]
        public User FindById(int id)
        {
            var user = _userService.FindByUser(id);
            return user;
        }
    }
}
